package dam.androidignacio.u4t8database.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import dam.androidignacio.u4t8database.model.Task;

public class TodoListDBManager {

    private final String NOT_STARTED = "Not Started";
    private final String IN_PROGRESS = "In Progress";
    private final String COMPLETED = "Completed";

    private TodoListDBHelper todoListDBHelper;
    private SQLiteDatabase sqLiteWritableDatabse;
    private SQLiteDatabase sqLiteReadableDatabase;

    public TodoListDBManager(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
        sqLiteWritableDatabse = todoListDBHelper.getWritableDatabase();
        sqLiteReadableDatabase = todoListDBHelper.getReadableDatabase();
    }


    // Operations

    // CREATE new row
    public void insert(String todo, String when, String description, String priority, String status) {
        // open database to read and write

        if (sqLiteWritableDatabse != null) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(TodoListDBContract.Tasks.TODO, todo);
            contentValues.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValues.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValues.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValues.put(TodoListDBContract.Tasks.STATUS, status);

            sqLiteWritableDatabse.insert(TodoListDBContract.Tasks.TABLE_NAME, null, contentValues);

            // https://developer.android.com/training/data-storage/sqlite#PersistingDbConnection:
            // getWritableDatabase() & getReadableDatabase() are expensive, you should leave your databse connetion
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }

    public void update(String id, String todo, String when, String description, String priority, String status) {

        if (sqLiteWritableDatabse != null) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(TodoListDBContract.Tasks.TODO, todo);
            contentValues.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValues.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValues.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValues.put(TodoListDBContract.Tasks.STATUS, status);

            String whereClause = String.format("%s=?", TodoListDBContract.Tasks._ID);
            String[] whereArgs = {id};

            sqLiteWritableDatabse.update(TodoListDBContract.Tasks.TABLE_NAME, contentValues, whereClause, whereArgs);
        }
    }

    public void delete(String id) {
        String whereClause = TodoListDBContract.Tasks._ID + "=?";
        String[] whereArgs = {id};
        sqLiteWritableDatabse.delete(TodoListDBContract.Tasks.TABLE_NAME, whereClause, whereArgs);
    }

    public void deleteCompleted() {
        String whereClause = TodoListDBContract.Tasks.STATUS + "=?";
        String[] wheerArgs = {COMPLETED};
        sqLiteWritableDatabse.delete(TodoListDBContract.Tasks.TABLE_NAME, whereClause, wheerArgs);
    }

    public void deleteAll() {
        sqLiteWritableDatabse.delete(TodoListDBContract.Tasks.TABLE_NAME, null, null);
    }

    // Get all data from Tasks table
    public ArrayList<Task> getTasks() {

        ArrayList<Task> taskList = new ArrayList<Task>();

        if (sqLiteReadableDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
                    TodoListDBContract.Tasks.TODO,
                    TodoListDBContract.Tasks.TO_ACCOMPLISH,
                    TodoListDBContract.Tasks.DESCRIPTION,
                    TodoListDBContract.Tasks.PRIORITY,
                    TodoListDBContract.Tasks.STATUS};

            Cursor cursorTodoList = sqLiteReadableDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group the rows
                    null,                    // don't filter by rou groups
                    null);                  // don't sort rows

            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                // read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }

        return taskList;

    }

    public ArrayList<Task> getTasksNotStarted() {

        ArrayList<Task> taskList = new ArrayList<Task>();

        if (sqLiteReadableDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
                    TodoListDBContract.Tasks.TODO,
                    TodoListDBContract.Tasks.TO_ACCOMPLISH,
                    TodoListDBContract.Tasks.DESCRIPTION,
                    TodoListDBContract.Tasks.PRIORITY,
                    TodoListDBContract.Tasks.STATUS};

            String whereClause = TodoListDBContract.Tasks.STATUS + "=?";
            String[] whereArgs = new String[] {NOT_STARTED};

            Cursor cursorTodoList = sqLiteReadableDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,                         // The columns toView return
                    whereClause,                        // WHERE clause
                    whereArgs,                          // values for the WHERE clause
                    null,                      // don't group the rows
                    null,                       // don't filter by rou groups
                    null);                     // don't sort rows

            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                // read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }

        return taskList;

    }

    public ArrayList<Task> getTasksInProgress() {

        ArrayList<Task> taskList = new ArrayList<Task>();

        if (sqLiteReadableDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
                    TodoListDBContract.Tasks.TODO,
                    TodoListDBContract.Tasks.TO_ACCOMPLISH,
                    TodoListDBContract.Tasks.DESCRIPTION,
                    TodoListDBContract.Tasks.PRIORITY,
                    TodoListDBContract.Tasks.STATUS};

            String whereClause = TodoListDBContract.Tasks.STATUS + "=?";
            String[] whereArgs = new String[] {IN_PROGRESS};

            Cursor cursorTodoList = sqLiteReadableDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,                         // The columns toView return
                    whereClause,                        // WHERE clause
                    whereArgs,                          // values for the WHERE clause
                    null,                      // don't group the rows
                    null,                       // don't filter by rou groups
                    null);                     // don't sort rows

            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                // read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }

        return taskList;

    }

    public ArrayList<Task> getTasksCompleted() {

        ArrayList<Task> taskList = new ArrayList<Task>();

        if (sqLiteReadableDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
                    TodoListDBContract.Tasks.TODO,
                    TodoListDBContract.Tasks.TO_ACCOMPLISH,
                    TodoListDBContract.Tasks.DESCRIPTION,
                    TodoListDBContract.Tasks.PRIORITY,
                    TodoListDBContract.Tasks.STATUS};

            String whereClause = TodoListDBContract.Tasks.STATUS + "=?";
            String[] whereArgs = new String[] {COMPLETED};

            Cursor cursorTodoList = sqLiteReadableDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,                         // The columns toView return
                    whereClause,                        // WHERE clause
                    whereArgs,                          // values for the WHERE clause
                    null,                      // don't group the rows
                    null,                       // don't filter by rou groups
                    null);                     // don't sort rows

            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                // read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }

        return taskList;

    }

    public void close() {
        todoListDBHelper.close();
    }

}
