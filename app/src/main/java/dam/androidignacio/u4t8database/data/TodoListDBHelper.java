package dam.androidignacio.u4t8database.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TodoListDBHelper extends SQLiteOpenHelper {
    // instance to SQLiteOpenHelper
    private static TodoListDBHelper instanceDBHelper;

    // This method assures only one instance of TodoListDBHelper for all the application.
    // https://www.androiddesignpatterns.com/2012/05/correctly-managing-your-sqlite-database.html
    // Use the application context, to not leak Activity context
    public static synchronized TodoListDBHelper getInstance(Context context) {
        // instance must be unique
        if (instanceDBHelper == null) {
            instanceDBHelper = new TodoListDBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }

    // Constructor should be private to prevent direct instantiation
    public TodoListDBHelper(Context context) {
        super(context, TodoListDBContract.DB_NAME, null, TodoListDBContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TodoListDBContract.Tasks.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // the upgrade policy is simply discard the table and start over
        db.execSQL(TodoListDBContract.Tasks.DELETE_TABLE);
        // create again the DB
        onCreate(db);
    }

}
