package dam.androidignacio.u4t8database;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import dam.androidignacio.u4t8database.data.TodoListDBManager;

public class AddTaskActivity extends AppCompatActivity {

    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spinnerPriority;
    private Spinner spinnerStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        setUI();
    }

    private void setUI() {
        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);
        spinnerPriority = findViewById(R.id.spinnerPriority);
        spinnerStatus = findViewById(R.id.spinnerStatus);

        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this, R.array.taskPpriority,
                android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(priorityAdapter);

        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(this, R.array.taskStatus,
                android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(statusAdapter);
    }

    public void onClick(View view) {

        if (view.getId() == R.id.buttonOk) {

            if (etTodo.getText().toString().length() > 0) {
                TodoListDBManager todoListDBManager = new TodoListDBManager(this);

                todoListDBManager.insert(etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString(),
                        spinnerPriority.getSelectedItem().toString(),
                        spinnerStatus.getSelectedItem().toString());

                Toast.makeText(this, "Task created successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.task_data_empty), Toast.LENGTH_SHORT).show();
            }

            finish();
        }
    }
}