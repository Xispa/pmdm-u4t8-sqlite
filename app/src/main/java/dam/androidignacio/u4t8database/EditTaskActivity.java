package dam.androidignacio.u4t8database;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import dam.androidignacio.u4t8database.data.TodoListDBContract;
import dam.androidignacio.u4t8database.data.TodoListDBManager;

public class EditTaskActivity extends AppCompatActivity {

    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spinnerPriority;
    private Spinner spinnerStatus;
    private Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        setUI();
    }

    private void setUI() {
        bundle = getIntent().getExtras();
        etTodo = findViewById(R.id.etEditTodo);
        etToAccomplish = findViewById(R.id.etEditToAccomplish);
        etDescription = findViewById(R.id.etEditDescription);
        spinnerPriority = findViewById(R.id.spinnerEditPriority);
        spinnerStatus = findViewById(R.id.spinnerEditStatus);

        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this, R.array.taskPpriority,
                android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(priorityAdapter);

        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(this, R.array.taskStatus,
                android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(statusAdapter);

        setData();
    }

    public void setData() {
        etTodo.setText(bundle.getString(TodoListDBContract.Tasks.TODO));
        etToAccomplish.setText(bundle.getString(TodoListDBContract.Tasks.TO_ACCOMPLISH));
        etDescription.setText(bundle.getString(TodoListDBContract.Tasks.DESCRIPTION));

        String[] priorities = getResources().getStringArray(R.array.taskPpriority);
        for (int i = 0; i < priorities.length; i++) {
            if (bundle.getString(TodoListDBContract.Tasks.PRIORITY).equals(priorities[i]))
                spinnerPriority.setSelection(i);
        }

        String[] statuses = getResources().getStringArray(R.array.taskStatus);
        for (int i = 0; i < statuses.length; i++) {
            if (bundle.getString(TodoListDBContract.Tasks.STATUS).equals(statuses[i]))
                spinnerStatus.setSelection(i);
        }
    }

    public void onClick(View view) {

        TodoListDBManager todoListDBManager = new TodoListDBManager(this);

        if (view.getId() == R.id.buttonSave) {

            if (etTodo.getText().toString().length() > 0) {
                todoListDBManager.update(bundle.getString(TodoListDBContract.Tasks._ID),
                        etTodo.getText().toString(),
                        etToAccomplish.getText().toString(),
                        etDescription.getText().toString(),
                        spinnerPriority.getSelectedItem().toString(),
                        spinnerStatus.getSelectedItem().toString());
                Toast.makeText(this, "Task updated successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.task_data_empty), Toast.LENGTH_SHORT).show();
            }

            finish();

        } else if (view.getId() == R.id.buttonDelete) {

            todoListDBManager.delete(bundle.getString(TodoListDBContract.Tasks._ID));
            Toast.makeText(this, "Task deleted succesfully", Toast.LENGTH_SHORT).show();
            finish();

        }
    }

}