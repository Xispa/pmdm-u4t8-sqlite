package dam.androidignacio.u4t8database;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuInflater;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import dam.androidignacio.u4t8database.data.TodoListDBContract;
import dam.androidignacio.u4t8database.data.TodoListDBManager;
import dam.androidignacio.u4t8database.model.Task;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    private RecyclerView rvTodoList;
    private TodoListDBManager todoListDBManager;
    private MyAdapter myAdapter;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get instance of our DB Manager
        todoListDBManager = new TodoListDBManager(this);
        myAdapter = new MyAdapter(todoListDBManager, this);

        setUI();

    }

    private void setUI() {
        // ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set fab: opens an activity to ADD A NEW TASK to the DB
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            // start activity to add a new record to our table
            startActivity(new Intent(getApplicationContext(), AddTaskActivity.class));
        });

        // set recyclerView
        rvTodoList = findViewById(R.id.rvTodoList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvTodoList.setAdapter(myAdapter);

        this.bundle = new Bundle();


    }

    @Override
    protected void onResume() {
        super.onResume();

        myAdapter.getData();
    }

    @Override
    protected void onDestroy() {
        // close any connection to DB
        todoListDBManager.close();

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        TodoListDBManager todoListDBManager = new TodoListDBManager(this);

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_quit: finish(); break;

            case R.id.action_show_not_started: myAdapter.setMyTaskList(todoListDBManager.getTasksNotStarted());
                                               myAdapter.notifyDataSetChanged(); break;
            case R.id.action_show_in_progress: myAdapter.setMyTaskList(todoListDBManager.getTasksInProgress());
                                               myAdapter.notifyDataSetChanged(); break;
            case R.id.action_show_completed: myAdapter.setMyTaskList(todoListDBManager.getTasksCompleted());
                                             myAdapter.notifyDataSetChanged(); break;
            case R.id.action_show_all: myAdapter.setMyTaskList(todoListDBManager.getTasks());
                                       myAdapter.notifyDataSetChanged(); break;

            case R.id.action_delete_completed: todoListDBManager.deleteCompleted();
                                               myAdapter.setMyTaskList(todoListDBManager.getTasks());
                                               myAdapter.notifyDataSetChanged(); break;

            case R.id.action_delete_all: todoListDBManager.deleteAll();
                                         myAdapter.setMyTaskList(todoListDBManager.getTasks());
                                         myAdapter.notifyDataSetChanged(); break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClickListener(Task task) {

        Intent intent = new Intent(this, EditTaskActivity.class);

        bundle.putString(TodoListDBContract.Tasks._ID, String.valueOf(task.get_id()));
        bundle.putString(TodoListDBContract.Tasks.TODO, task.getTodo());
        bundle.putString(TodoListDBContract.Tasks.TO_ACCOMPLISH, task.getToAccomplish());
        bundle.putString(TodoListDBContract.Tasks.DESCRIPTION, task.getDescription());
        bundle.putString(TodoListDBContract.Tasks.PRIORITY, task.getPriority());
        bundle.putString(TodoListDBContract.Tasks.STATUS, task.getStatus());

        intent.putExtras(bundle);
        startActivity(intent);

    }
}