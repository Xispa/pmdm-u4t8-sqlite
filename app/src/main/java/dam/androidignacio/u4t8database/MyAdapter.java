package dam.androidignacio.u4t8database;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidignacio.u4t8database.data.TodoListDBManager;
import dam.androidignacio.u4t8database.model.Task;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private TodoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;
    private OnItemClickListener listener;

    // Class for each item
    static class MyViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout constraintLayout;
        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;
        TextView tvPriority;
        TextView tvStatus;

        public MyViewHolder(View view) {
            super(view);

            this.constraintLayout = (ConstraintLayout) view;
            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
            this.tvPriority = view.findViewById(R.id.tvPriorityTask);
            this.tvStatus = view.findViewById(R.id.tvStatusTask);
        }

        // sets viewHolder view with data
        public void bind(Task task, OnItemClickListener clickListener) {
            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescription());
            this.tvPriority.setText(task.getPriority());
            this.tvStatus.setText(task.getStatus());

            this.constraintLayout.setOnClickListener(v -> {clickListener.onItemClickListener(task);});
        }
    }

    public void setMyTaskList(ArrayList<Task> myTaskList) {
        this.myTaskList = myTaskList;
    }

    public interface OnItemClickListener {
        void onItemClickListener(Task itemSelected);
    }

    // constructor: todoListDBManager gets DB data
    public MyAdapter(TodoListDBManager todoListDBManager, OnItemClickListener listener) {
        this.todoListDBManager = todoListDBManager;
        this.listener = listener;
    }

    // get data from DB
    public void getData() {
        this.myTaskList = todoListDBManager.getTasks();
        notifyDataSetChanged();
    }

    // Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create item View:
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_item, parent, false);

        return new MyViewHolder(itemLayout);
    }

    // replaces the data content of a viewholder (recycles old viewholder): Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind vieHolder with data at: position
        viewHolder.bind(myTaskList.get(position), listener);
    }

    // returns the size of dataSet: Layout Manager calls this method
    @Override
    public int getItemCount() {
        return myTaskList.size();
    }
}
